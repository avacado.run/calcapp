#include <iostream>
#include "Helpers.h"

int SquareSum()
{
    std::cout << "Input first number: ";
    int FirstNumber;
    std::cin >> FirstNumber;

    std::cout << "Input second number: ";
    int SecondNumber;
    std::cin >> SecondNumber;

    return  GetSquareSum(FirstNumber, SecondNumber);
}

int main(int argc, char* argv[])
{
    std::cout << "The square of the sum: " << SquareSum() << std::endl;
    return 0;
}
