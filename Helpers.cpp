﻿#include <cmath>

int GetSquareSum(int a, int b)
{
    return pow(a + b, 2);
}